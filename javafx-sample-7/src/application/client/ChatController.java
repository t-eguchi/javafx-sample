package application.client;

import java.net.URL;
import java.util.ResourceBundle;

import org.eclipse.jetty.websocket.WebSocket.OnTextMessage;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * チャットクライアントのコントローラオブジェクトです。
 * @author t-eguchi
 *
 */
public class ChatController implements Initializable {

	/** 送信テキスト */
	@FXML
	private TextArea sendText;

	/** 受信テキスト */
	@FXML
	private TextArea rcvText;

	/** ボタン */
	@FXML
	private Button button;

	/** クライアント */
	private ChatWebSocketClient client;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// 受信時の挙動
		OnTextMessage handler = new OnTextMessage() {

			@Override
			public void onOpen(Connection connection) {}

			@Override
			public void onClose(int closeCode, String message) {}

			@Override
			public void onMessage(String data) {
				String text = rcvText.getText();
				text = text + data + "\n";
				rcvText.setText(text);
			}
		};
		this.client = new ChatWebSocketClient(handler);

		// 送信時の挙動
		this.button.setOnAction((event) -> {
			String msg = sendText.getText();
			this.client.sendMessage(msg);
			sendText.clear();
		});
	}
}
