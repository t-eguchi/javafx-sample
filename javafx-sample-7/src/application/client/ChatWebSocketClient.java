package application.client;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocket.Connection;

import application.WebSocketUtil;

/**
 * チャットを行う際のクライアントです。
 * @author t-eguchi
 */
public class ChatWebSocketClient {

	/** コネクション */
	private final Connection connection;

	/** クライアントマネージャ */
	private final WebSocketConnectionManager manager = WebSocketConnectionManager.getInstance();

	/**
	 * ハンドラを渡すことによりインスタンスを生成します。
	 * @param handler
	 */
	public ChatWebSocketClient(WebSocket handler) {
		this("ws://localhost:8080/ws/", handler);
	}

	/**
	 * URLとハンドラを渡すことによりインスタンスを生成します。
	 * @param url
	 * @param handler
	 */
	public ChatWebSocketClient(String url, WebSocket handler) {
		// クライアントの生成
		this.connection = this.manager.open(url, handler);
	}

	/**
	 * メッセージを送信します。
	 * @param msg
	 */
	public void sendMessage(String msg) {
		WebSocketUtil.sendMessage(this.connection, msg);
	}

	/**
	 * 切断します。
	 */
	public void disconnect() {
		this.connection.disconnect();
	}
}
