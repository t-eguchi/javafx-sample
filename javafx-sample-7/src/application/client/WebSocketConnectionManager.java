package application.client;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocket.Connection;
import org.eclipse.jetty.websocket.WebSocketClient;
import org.eclipse.jetty.websocket.WebSocketClientFactory;

import application.WebSocketUtil;

/**
 * 接続を管理するクラスです。
 * @author t-eguchi
 *
 */
public final class WebSocketConnectionManager {

	/** コネクションリスト */
	private final List<Connection> connections = new LinkedList<>();

	/** ファクトリ */
	private final WebSocketClientFactory factory;

	/** インスタンス */
	private static final WebSocketConnectionManager instance = new WebSocketConnectionManager();
	public static WebSocketConnectionManager getInstance() {
		return instance;
	}

	/** デフォルトコンストラクタを隠蔽 */
	private WebSocketConnectionManager() {
		this.factory = new WebSocketClientFactory();
		WebSocketUtil.start(this.factory);
	}

	/**
	 * クライアントを作成します。
	 * @param url
	 * @param handler
	 * @return
	 */
	public Connection open(String url, WebSocket handler) {
		// コネクションをコレクションに追加
		WebSocketClient client = this.factory.newWebSocketClient();
		URI uri = WebSocketUtil.createURI(url);
		Future<Connection> future = WebSocketUtil.open(client, uri, handler);
		try {
			Connection connection = future.get();
			this.connections.add(connection);
			return connection;
		}
		catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * コネクションをクローズします。
	 */
	public void close() {
		this.connections.forEach(connection -> {
			connection.disconnect();
		});
	}

	/**
	 * WebSocketClientFactoryをクローズします。
	 */
	public void stop() {
		WebSocketUtil.stop(this.factory);
	}

	/**
	 * コンポーネントの破棄を行います。
	 */
	public void destroy() {
		close();
		stop();
		this.factory.destroy();
	}
}
