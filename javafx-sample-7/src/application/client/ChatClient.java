package application.client;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ChatClient extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// ペイン
		URL resource = getClass().getResource("ChatClient.fxml");
		BorderPane pane = FXMLLoader.load(resource);

		// シーン
		Scene scene = new Scene(pane, 200, 200);

		// 表示
		stage.setScene(scene);
		stage.setOnCloseRequest(value -> {
			// クローズ
			WebSocketConnectionManager manager = WebSocketConnectionManager.getInstance();
			manager.destroy();
		});
		stage.show();
	}
}
