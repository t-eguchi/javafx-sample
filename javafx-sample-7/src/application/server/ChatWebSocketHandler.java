package application.server;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jetty.websocket.WebSocket.OnTextMessage;

import application.WebSocketUtil;

/**
 * コネクションが確立した際のイベントハンドラです。
 * @author t-eguchi
 *
 */
public class ChatWebSocketHandler implements OnTextMessage {

	/** コネクション */
	private Connection connection;

	/** ソケットメンバー(マルチスレッドセーフではない) */
	private static final Set<Connection> group = new HashSet<>();

	@Override
	public void onOpen(Connection connection) {
		this.connection = connection;
		group.add(connection);
	}

	@Override
	public void onClose(int closeCode, String message) {
		group.remove(this.connection);
		System.err.println("close connection.");
	}

	@Override
	public void onMessage(String data) {
		// グループにメッセージを送信
		for (Connection connection : group) {
			WebSocketUtil.sendMessage(connection, data);
		}
	}

}
