package application.server;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * チャットを行うWebSocketサーバです。
 * @author t-eguchi
 */
public class ChatWebSocketServer {

	/** サーバ */
	private final Server server;

	/**
	 * ポート番号を渡すことにより、インスタンスを生成します。
	 * @param port
	 */
	public ChatWebSocketServer(int port) {
		this(port, "/ws/*");
	}

	/**
	 * ポート番号を渡すことにより、インスタンスを生成します。
	 * @param port
	 */
	public ChatWebSocketServer(int port, String path) {
		this.server = new Server(port);

		// サーブレットの登録
		ChatWebSocketServlet servlet = new ChatWebSocketServlet();
	    ServletHolder serbletHoler = new ServletHolder(servlet);
	    ServletContextHandler handler = new ServletContextHandler();
	    handler.addServlet(serbletHoler, path);
	    HandlerList handlerList = new HandlerList();
	    handlerList.setHandlers(new Handler[] {handler});
	    this.server.setHandler(handlerList);
	}

	/**
	 * 開始します。
	 */
	public void start() {
		try {
			this.server.start();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 停止します。
	 */
	public void stop() {
		try {
			this.server.stop();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		// サーバ起動
		ChatWebSocketServer server = new ChatWebSocketServer(8080);
		server.start();

		// シャットダウン時の処理
		Runnable shutdownHook = new Runnable() {
			@Override
			public void run() {
				System.err.println("停止します。");
				server.stop();
			}
		};
		Thread shutdownThread = new Thread(shutdownHook);
		Runtime.getRuntime().addShutdownHook(shutdownThread);
	}
}
