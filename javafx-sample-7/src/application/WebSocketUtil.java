package application;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Future;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocket.Connection;
import org.eclipse.jetty.websocket.WebSocketClient;
import org.eclipse.jetty.websocket.WebSocketClientFactory;

/**
 * WebSocketを扱う際のユーティリティクラスです。
 * @author t-eguchi
 *
 */
public final class WebSocketUtil {

	/** デフォルトコンストラクタを隠蔽 */
	private WebSocketUtil() {}

//	public static void sendMessage(Outbound outbound, byte frame, String data) {
//		try {
//			outbound.sendMessage(frame, data);
//		}
//		catch (IOException e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	public static void sendMessage(Outbound outbound,
//			byte frame, byte[] data, int offset, int length) {
//		try {
//			outbound.sendMessage(frame, data, offset, length);
//		}
//		catch (IOException e) {
//			throw new RuntimeException(e);
//		}
//	}

	public static void sendMessage(Connection connection, String data) {
		try {
			connection.sendMessage(data);
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void start(WebSocketClientFactory factory) {
		if (factory.isStarted()) {
			return;
		}

		try {
			factory.start();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static Future<Connection> open(WebSocketClient client, URI uri, WebSocket socket) {
		try {
			return client.open(uri, socket);
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static URI createURI(String uri) {
		try {
			return new URI(uri);
		}
		catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public static void stop(WebSocketClientFactory factory) {
		if (factory.isStopped()) {
			return;
		}

		try {
			factory.stop();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
