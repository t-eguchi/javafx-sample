package application.sample3;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class SampleController implements Initializable {

	/** ラベルをバインド */
	@FXML
	private Label label1;

	/** テキストフィールドをバインド */
	@FXML
	private TextField field1;

	/** ボタンをバインド */
	@FXML
	private Button button1;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.button1.setOnAction((event) -> {
			String text = this.field1.getText();
			text = "あなたは" + text + "と入力しました。";
			this.label1.setText(text);
		});
	}
}
