package application.sample3;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class FXMLSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// クラスパスより読み込む
		URL url = getClass().getResource("FXMLSample.fxml");
		BorderPane pane = (BorderPane)FXMLLoader.load(url);

		// シーン
		Scene scene = new Scene(pane, 320, 120);

		// 表示
		stage.setScene(scene);
		stage.show();
	}
}
