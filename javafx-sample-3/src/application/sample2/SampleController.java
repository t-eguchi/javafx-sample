package application.sample2;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class SampleController {

	/** ラベルをバインド */
	@FXML
	private Label label1;

	/** テキストフィールドをバインド */
	@FXML
	private TextField field1;

	@FXML
	public void doAction(ActionEvent event) {
		String text = this.field1.getText();
		text = "あなたは" + text + "と入力しました。";
		this.label1.setText(text);
	}
}
