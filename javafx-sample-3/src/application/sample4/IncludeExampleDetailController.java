package application.sample4;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class IncludeExampleDetailController {

	/** カテゴリラベル */
	@FXML
	private Label category;

	/** 名前 */
	@FXML
	private Label name;

	/** 説明 */
	@FXML
	private TextArea description;

	private Product product;

	private ChangeListener<String> listener;

	public void setProduct(Product product) {
		if (this.product != null) {
			unhookListener();
		}
		this.product = product;
		hookTo(product);
	}

	private void unhookListener() {
		this.description.textProperty().removeListener(this.listener);
	}

	private void hookTo(Product product) {
		if (product == null) {
			this.category.setText("");
			this.name.setText("");
			this.description.setText("");
			this.listener = null;
			return;
		}

		this.category.setText(product.getCategory());
		this.name.setText(product.getName());
		this.description.setText(product.getDescription());
		this.description.textProperty().addListener(this.listener);
	}
}
