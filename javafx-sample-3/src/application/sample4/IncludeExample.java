package application.sample4;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class IncludeExample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// ペインのロード
		FXMLLoader loader = new FXMLLoader();
		URL url = getClass().getResource("IncludeExampleTree.fxml");
		loader.setLocation(url);
		BorderPane pane = loader.load();

		// 表示
		Scene scene = new Scene(pane, 600, 400);
		stage.setTitle("Include Example");
		stage.setScene(scene);
		stage.show();
	}
}
