package application.sample4;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.VBox;

public class IncludeExampleTreeController {

	@FXML
	private TreeTableView<Product> treeTableView;

	@FXML
	private TreeTableColumn<Product, String> category;

	@FXML
	private TreeTableColumn<Product, String> name;

	@FXML
	private VBox details;

	@FXML
	private IncludeExampleDetailController detailsController;

	@FXML
	public void initialize() {
		// 商品の初期化
		Product[] products = new Product[101];
		for (int i = 0; i < products.length; i++) {
			products[i] = new Product();
			products[i].setCategory("Category" + ( i / 10));
			products[i].setName("Name" + i);
			products[i].setDescription("Description" + i);
		}

		// ツリーの初期化
		TreeItem<Product> root = new TreeItem<>(products[100]);
		root.setExpanded(true);
		for (int i = 0; i < 10; i++) {
			TreeItem<Product> firstLevel = new TreeItem<>(products[i * 10]);
			firstLevel.setExpanded(true);
			for (int j = 0; j < 10; j++) {
				TreeItem<Product> secondLevel = new TreeItem<>(products[i * 10 + j]);
				secondLevel.setExpanded(true);
				firstLevel.getChildren().add(secondLevel);
			}
			root.getChildren().add(firstLevel);
		}

		// イベント登録
		this.category.setCellValueFactory(param -> {
			return new ReadOnlyStringWrapper(param.getValue().getValue().getCategory());
		});
		this.name.setCellValueFactory(param -> {
			return new ReadOnlyStringWrapper(param.getValue().getValue().getName());
		});

		// ツリーテーブルの初期化
		this.treeTableView.setRoot(root);
		this.treeTableView.getSelectionModel().selectedItemProperty()
			.addListener((observable, oldValue, newValue) -> {
				Product product = null;
				if (newValue != null) {
					product = newValue.getValue();
				}
				this.detailsController.setProduct(product);;
		});
	}
}
