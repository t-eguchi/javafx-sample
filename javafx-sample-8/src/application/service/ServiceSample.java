package application.service;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Taskを利用したサンプルアプリケーションです。
 * @author t-eguchi
 *
 */
public class ServiceSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		BorderPane root = new BorderPane();

		// ラベル
		Label label = new Label("None.");
		root.setTop(label);

		// ボタン
		Button button = new Button("Push");
		root.setBottom(button);


		// イベント追加(再利用できない一度きりの実行)
		Task<Boolean> task = new Task<Boolean>() {

			/** カウンタ */
			private int count = 0;

			@Override
			protected Boolean call() throws Exception {
				Thread.sleep(5000);

				// 描画
				Platform.runLater(() -> {
					label.setText("Pushed!! (" + (++this.count) + ")");
				});

				return true;
			}
		};
		button.setOnAction(value -> {
			label.setText("Pushing...");
			Thread thread = new Thread(task);
			thread.start();
		});

		// 表示
		Scene scene = new Scene(root, 200, 200);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
