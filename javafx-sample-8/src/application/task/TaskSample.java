package application.task;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Serviceを利用したサンプルアプリケーションです。
 * @author t-eguchi
 *
 */
public class TaskSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		BorderPane root = new BorderPane();

		// ラベル
		Label label = new Label("None.");
		root.setTop(label);

		// ボタン
		Button button = new Button("Push");
		root.setBottom(button);

		// イベント追加(再利用可能)
		Service<Boolean> service = new Service<Boolean>() {

			/** カウンタ */
			private int count = 0;

			@Override
			protected Task<Boolean> createTask() {
				Task<Boolean> task = new Task<Boolean>() {
					@Override
					protected Boolean call() throws Exception {
						Thread.sleep(5000);

						// 描画
						Platform.runLater(() -> {
							label.setText("Pushed!! (" + (++count) + ")");
						});

						return true;
					}
				};
				return task;
			}
		};
		button.setOnAction(value -> {
			label.setText("Pushing...");
			service.restart();
		});

		// 表示
		Scene scene = new Scene(root, 200, 200);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
