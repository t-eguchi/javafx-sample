package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Sample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// ペインの作成
		BorderPane pane = new BorderPane();

		// ラベルの追加
		Label label = new Label("JavaFX Sample");
		pane.setTop(label);

		// テキストの追加
		TextField textField = new TextField();
		textField.setPromptText("テキストを入力してください。");
		pane.setCenter(textField);

		// ボタンの追加
		Button button = new Button("Click");
		button.setOnAction(event -> {
			String msg = textField.getText();
			label.setText(msg);
		});
		pane.setBottom(button);

		// シーンの作成
		Scene scene = new Scene(pane, 320, 120);
		stage.setScene(scene);
		stage.show();
	}
}
