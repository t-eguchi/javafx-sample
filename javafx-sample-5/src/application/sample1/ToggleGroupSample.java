package application.sample1;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ToggleGroupSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		URL url = getClass().getResource("ToggleGroupSample.fxml");
		BorderPane pane = FXMLLoader.load(url);
		Scene scene = new Scene(pane, 320, 120);

		// 表示
		stage.setScene(scene);
		stage.show();
	}

}
