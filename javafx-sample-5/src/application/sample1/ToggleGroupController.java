package application.sample1;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;

public class ToggleGroupController implements Initializable {

	@FXML
	private Label label;

	@FXML
	private ToggleGroup group;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// イベントハンドラ
		ReadOnlyObjectProperty<Toggle> property = this.group.selectedToggleProperty();
		property.addListener((event, oldVal, newVal) -> {
			this.label.setText(oldVal.getUserData() + " -> " + newVal.getUserData());
		});
	}
}
