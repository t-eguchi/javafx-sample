package application.sample2;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;

public class ComboBoxController implements Initializable {

	@FXML
	private Label label;

	@FXML
	private ComboBox<String> combo;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// イベントハンドラ
		SingleSelectionModel<String> model = this.combo.getSelectionModel();
		ReadOnlyObjectProperty<String> property = model.selectedItemProperty();
		property.addListener((event, oldVal, newVal) -> {
			this.label.setText(oldVal + " -> " + newVal);
		});
	}
}
