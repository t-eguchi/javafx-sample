package application.sample3;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class SliderController implements Initializable {

	@FXML
	private Label label;

	@FXML
	private Slider slider;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// イベントハンドラ
		DoubleProperty property = this.slider.valueProperty();
		property.addListener((event, oldVal, newVal) -> {
			this.label.setText(oldVal + " -> " + newVal);
		});
	}
}
