package application.canvas;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class CanvasSampleController implements Initializable {

	@FXML
	private Canvas canvas;

	private GraphicsContext context;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.context = this.canvas.getGraphicsContext2D();

		// 四角形の描画
		this.context.setFill(Color.RED);
		this.context.fillRect(50, 50, 150, 150);

		// 楕円の描画
		this.context.setStroke(Color.BLUE);
		this.context.strokeOval(100, 100, 50, 50);
	}


}
