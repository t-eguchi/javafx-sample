package application.canvas;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class CanvasSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// レイアウト読み込み
		URL resource = getClass().getResource("CanvasSample.fxml");
		BorderPane pane = FXMLLoader.load(resource);

		// 描画
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}
}
