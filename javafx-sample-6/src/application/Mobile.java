package application;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class Mobile extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		// FXML読み込み
		URL fxmlRes = getClass().getResource("mobile.fxml");
		BorderPane pane = FXMLLoader.load(fxmlRes);
		Scene scene = new Scene(pane, 180, 250);

		// CSS設定
		URL cssRes = getClass().getResource("mobile.css");
		String cssForm = cssRes.toExternalForm();
		ObservableList<String> styleSheets = scene.getStylesheets();
		styleSheets.add(cssForm);

		// 表示
		stage.setScene(scene);
		stage.show();
	}
}
