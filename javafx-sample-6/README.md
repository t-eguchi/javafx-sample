# README #

CSSを利用したサンプルです。
またビルド方法についてもサンプルとなります。

## CSSの文法 ##

### idにスタイルを適用 ###

~~~css
#my_id {
	:
}
~~~

### classにスタイルを適用 ###

~~~css
.my_class {
	:
}
~~~

### 特定の親クラスの場合にスタイルを適用 ###

~~~css
.parent_class > .child_class {
	:
}
~~~

### 複数のクラスに同一のスタイルを適用 ###

~~~css
.class_1, .class_2 {
	:
}
~~~

### クラスの状態に対して、スタイルを適用 ###

~~~css
.button:state {
	:
}
~~~

状態(Button) : hover / selected / focused

## ビルド方法 ##

### EXEの作成方法 ###

- http://www.jrsoftware.org より **Inno Setup** をインストールする。

- **iscc.exe** コマンドが実行できるように PATH を通す。

- **build.fxbuild** の **Packaging Format** を **exe** に指定する。

- **Generate ant build.xml and run** を実行する。

### MSIの作成方法 ###

- http://wix.sf.net より **WiXツール** をインストールする。

- **light.exe** コマンドが実行できるように PATH を通す。

- **build.fxbuild** の **Packaging Format** を **msi** に指定する。

- **Generate ant build.xml and run** を実行する。
