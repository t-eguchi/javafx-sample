package application.sample2;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;

public class ListViewController implements Initializable {

	@FXML
	private Label label1;

	@FXML
	private ListView<String> list1;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// アイテムの登録
		ObservableList<String> items = this.list1.getItems();
		items.add("Windows 10");
		items.add("Mac OS");
		items.add("Linux (CentOS)");

		// イベントの登録
		this.list1.setOnMouseClicked((event) -> {
			MultipleSelectionModel<String> model = this.list1.getSelectionModel();
			String text = model.getSelectedItem();
			this.label1.setText(text);
		});
	}
}
