package application;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ComboBoxSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// ラベル
		Label label = new Label("This is JavaFX.");
		BorderPane pane = new BorderPane();
		pane.setTop(label);

		// コンボボックス
		ObservableList<String> comboValues = FXCollections.observableArrayList(
				"One", "Two", "Three");
		ComboBox<String> comboBox = new ComboBox<>(comboValues);
		pane.setCenter(comboBox);
		comboBox.setOnAction((event) -> {
			String value = comboBox.getValue();
			label.setText(value);
		});
		comboBox.setEditable(true);

		// シーン
		Scene scene = new Scene(pane, 320, 120);
		stage.setScene(scene);
		stage.show();
	}

}
