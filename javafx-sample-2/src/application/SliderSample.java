package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class SliderSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// ラベル
		Label label = new Label("This is JavaFX.");
		BorderPane pane = new BorderPane();
		pane.setTop(label);

		// スライダー
		Slider slider = new Slider(0, 100, 50);
		slider.setShowTickMarks(true);	// 区切りを表示
		slider.setShowTickLabels(true);	// 数値ラベルの表示
		slider.setSnapToTicks(true);	// 値を区切りに合わせる
		pane.setCenter(slider);

		// ボタン
		Button button = new Button("Click");
		button.setOnAction((event) -> {
			double value = slider.getValue();
			label.setText("value = " + value);
		});
		pane.setBottom(button);

		// 表示
		Scene scene = new Scene(pane, 320, 120);
		stage.setScene(scene);
		stage.show();
	}
}
