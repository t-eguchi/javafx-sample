package application;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class RadioButtonSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("RadioButton");

		// ラベル
		Label label = new Label("This is JavaFX.");
		BorderPane mainPain = new BorderPane();
		mainPain.setTop(label);

		// ラジオボタン
		RadioButton maleButton = new RadioButton("Male");
		maleButton.setSelected(true);
		maleButton.setOnAction((event) -> {
			label.setText("You're Male!!");
		});
		ToggleGroup group = new ToggleGroup();
		maleButton.setToggleGroup(group);

		RadioButton femaleButton = new RadioButton("Female");
		femaleButton.setOnAction((event) -> {
			label.setText("You're Female!!");
		});
		femaleButton.setToggleGroup(group);

		// ラジオペイン
		FlowPane radioPane = new FlowPane();
		ObservableList<Node> children = radioPane.getChildren();
		children.add(maleButton);
		children.add(femaleButton);
		mainPain.setCenter(radioPane);

		// シーン
		Scene scene = new Scene(mainPain, 320, 120);
		stage.setScene(scene);
		stage.show();
	}

}
