package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class CheckBoxSample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("CheckBox");

		// ラベル
		Label label = new Label("This is JavaFX");
		BorderPane pane = new BorderPane();
		pane.setTop(label);

		// チェックボックス
		CheckBox checkBox = new CheckBox("チェックボックス");
		checkBox.setOnAction((event) -> {
			String msg = checkBox.isSelected() ? "Selected!!" : "Not Selected.";
			label.setText(msg);
		});
		pane.setCenter(checkBox);

		// シーン
		Scene scene = new Scene(pane, 320, 120);
		stage.setScene(scene);
		stage.show();
	}
}
